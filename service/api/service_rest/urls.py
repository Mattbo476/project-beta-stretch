from django.urls import path
from .views import appointment_list, tech_list, appointment_details

urlpatterns = [
    path('appointments/', appointment_list, name='appointment_list'),
    path('technicians/', tech_list, name='tech_list'),
    path('appointments/<int:id>', appointment_details, name='appointment_details'),

]
