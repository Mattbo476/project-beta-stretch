from common.json import ModelEncoder
from .models import Appointment, Technician


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        'name',
        'employee_num',
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'vin',
        'customer_name',
        'date',
        'time',
        'reason',
        'status',
        'vip',
        'id',
        'technician',
    ]
    encoders = {
        'technician': TechnicianEncoder(),
    }
