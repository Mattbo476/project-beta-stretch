from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)

class SalesPerson(models.Model):
    name = models.CharField(max_length=255)
    employee_number = models.PositiveSmallIntegerField()

class Customer(models.Model):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=12)

class Sales(models.Model):
    salesperson = models.ForeignKey(SalesPerson, related_name = "salesperson", on_delete=models.PROTECT)
    customer = models.ForeignKey(Customer, related_name = "customer", on_delete=models.CASCADE)
    price = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17)
