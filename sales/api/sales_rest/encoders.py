from common.json import ModelEncoder
from .models import AutomobileVO, SalesPerson, Customer, Sales


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin',
        'import_href',
    ]

class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]

class SalesEncoder(ModelEncoder):
    model = Sales
    properties = [
        "salesperson",
        "customer",
        "price",
        "vin",
    ]
    encoders= {
        "salesperson": SalesPersonListEncoder(),
        "customer": CustomerEncoder(),
    }
