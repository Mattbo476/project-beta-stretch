from django.urls import path
from .views import sales_person_list, customer_list, sales_list

urlpatterns = [
    path('sales_people/', sales_person_list, name = 'sales_person_list'),
    path('customers/', customer_list, name = 'customer_list'),
    path('sales/', sales_list, name = 'sales_list')
]
