import React, { useEffect, useState } from 'react';

function VehicleModelList() {
    const [models, setModels] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        const data = await response.json();
        setModels(data.models)

    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <div>
            <h1>Vehicle Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(models => {
                        return (
                            <tr key={models.id}>
                                <td>{models.name}</td>
                                <td>{models.manufacturer.name}</td>
                                <td><img src={models.picture_url}
                                alt="car-pic"
                                width="320"
                                height="200" /></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default VehicleModelList;
