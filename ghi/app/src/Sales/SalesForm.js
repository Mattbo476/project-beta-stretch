import React, { useState, useEffect } from 'react';

function SalesForm(){
    const [salesPeople, setSalesPeople] = useState([])
    const [customers, setCustomers] = useState([])
    const [automobiles, setAutomobiles] = useState([])

    const [formData, setFormData] = useState({
        salesperson: '',
        customer: '',
        vin: '',
        price: ''
    })

    const getSalesPeople = async () => {
        const url = 'http://localhost:8090/api/sales_people/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.sales_person);
        }
    }

    const getCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customer);
        }
    }

    const getAutomobiles = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }

    useEffect(() => {
        getSalesPeople()
        getCustomers()
        getAutomobiles()
    }, [])

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salesUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                salesperson: '',
                customer: '',
                vin: '',
                price: ''
            });
        }
    }

    return (
        <div>
            <h1>Record a New Sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
                <select onChange={handleFormChange}
                    value={formData.vin}
                    required name="vin"
                    id="vin"
                    className="form-select">
                    <option value="">Choose an Automobile</option>
                        {automobiles.map(automobile => {
                            return (
                                <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                                )
                        })}
                </select>
                <select onChange={handleFormChange}
                    value={formData.salesperson}
                    required name="salesperson"
                    id="salesperson"
                    className="form-select">
                    <option value="">Choose a Sales Person</option>
                        {salesPeople.map(sales_person => {
                            return (
                                <option key={sales_person.id} value={sales_person.id}>{sales_person.name}</option>
                                )
                        })}
                </select>
                <select onChange={handleFormChange}
                    value={formData.customer}
                    required name="customer"
                    id="customer"
                    className="form-select">
                    <option value="">Choose a Customer</option>
                        {customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.id}>{customer.name}</option>
                                )
                        })}
                </select>
                <input onChange={handleFormChange}
                    value={formData.price}
                    placeholder="Sale Price"
                    required type="number"
                    name="price" id="price"
                    className="form-control" />
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    )
}

export default SalesForm
