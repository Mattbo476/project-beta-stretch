import React, { useEffect, useState } from 'react';

function SalesList() {
    const [sales, setSales] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        const data = await response.json();
        setSales(data.sales)
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <div>
            <h1>Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Salesperson</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.vin}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.salesperson.name}</td>
                                <td>{sale.price}</td>
                            </tr>
                );
                    })}
            </tbody>
        </table>
        </div >
    );
}

export default SalesList;
